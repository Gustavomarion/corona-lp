import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ComoFuncionaComponent } from './pages/como-funciona/como-funciona.component';
import { QueroParticiparComponent } from './pages/quero-participar/quero-participar.component';
import { AgeComponent } from './pages/age/age.component';
import { ObrigadoComponent } from './pages/obrigado/obrigado.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};
import { AgmCoreModule } from '@agm/core';
import { HelperService } from './helper.service';
// 
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, SatDatepickerModule } from 'saturn-datepicker'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter'
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { SharedModule } from './pages/shared.module';
registerLocaleData(localePt, 'pt-BR');
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComoFuncionaComponent,
    QueroParticiparComponent,
    AgeComponent,
    ObrigadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDK2rr8AG9S3Cs-NX63reTRCAZe_l4dHTY',
      libraries: ['geometry', 'places']
    }),
    SatDatepickerModule,
    SharedModule

  ],
  providers: [
    HelperService,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
