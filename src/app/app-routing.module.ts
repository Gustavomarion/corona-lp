import { EmBreveComponent } from './pages/em-breve/em-breve.component';
import { ObrigadoComponent } from './pages/obrigado/obrigado.component';
import { AgeComponent } from './pages/age/age.component';
import { QueroParticiparComponent } from './pages/quero-participar/quero-participar.component';
import { ComoFuncionaComponent } from './pages/como-funciona/como-funciona.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'age' },
  // { path: 'home', component: HomeComponent },
  // { path: 'como-funciona', component: ComoFuncionaComponent },
  // { path: 'quero-participar', component: QueroParticiparComponent },
  { path: 'age', component: AgeComponent },
  { path: 'home', component: ObrigadoComponent },
  // { path: 'em-breve', component: EmBreveComponent },
  // { path: 'fluxembauer', component: HomeComponent },
  // { path: 'fluxembauer/como-funciona', component: ComoFuncionaComponent },
  // { path: 'fluxembauer/quero-participar', component: QueroParticiparComponent },
  // { path: 'fluxembauer/age', component: AgeComponent },
  // { path: 'fluxembauer/obrigado', component: ObrigadoComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
