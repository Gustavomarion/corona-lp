import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VoucherService } from 'src/app/voucher.service';
import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from "@agm/core";
import { HelperService } from 'src/app/helper.service';
declare const google: any;
import { debounceTime, distinct } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
@Component({
    selector: 'app-quero-participar',
    templateUrl: './quero-participar.component.html',
    styleUrls: ['./quero-participar.component.scss'],
    providers: [VoucherService]
})
export class QueroParticiparComponent implements OnInit {
    public form: FormGroup = this.fb.group({
        origin_city: ['', Validators.required],
        origin_date: ['', Validators.required],
        destination_city: ['', Validators.required],
        destination_date: ['', Validators.required],
        file: ['', Validators.required],
        birthdate: ['', Validators.required],
        name: ['', Validators.required],
        cell_phone: ['', Validators.required],
        email: ['', Validators.required],
        address: ['', Validators.required],
        km_distance: ['', Validators.required],
        km_distance_public: [''],
        cpf: ['', Validators.required],

        aggree_policy: [''],
        aggree_share: [''],
        aggree_prom: ['']
    });

    public loading: boolean;

    public cities: any = {
        from: {},
        to: {}
    };
    public blur: boolean;

    public maxBirthdate = this.helper.date().add(-18, 'years').format();
    public minDateDestination: any;
    public minDateOrigin = this.helper.date([2020, 1, 21]).format();
    public maxDateOrigin: any;
    public maxDateDestination = this.helper.date([2020, 2, 1]).format();
    constructor(
        private fb: FormBuilder,
        private voucherService: VoucherService,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        private helper: HelperService,
        private router: Router
    ) {

    }


    ngOnInit() {
        //    this.calcKm('from', 'sao paulo');
        this.maxDateOrigin = this.maxDateDestination;
        this.form.get('origin_date').setValue(this.helper.date().format());
        this.selectDate({value: this.helper.date()})

        this.form.get('cpf').valueChanges.pipe(
            debounceTime(300)
        ).subscribe((value: any) => {
            if(!this.helper.validateCpf(value)) {
                this.form.get('cpf').setErrors({cpf_invalid: true});
            } else {
                this.form.get('cpf').setErrors(null);
            }

            console.log(this.form)
        });



        this.form.get('km_distance_public').valueChanges.subscribe((value: any) => {
            // this.form.get('km_distance').setValue(value * 1000);
        });


        // this.form.get('destination_date').valueChanges.subscribe(() => this.validateDates())
        // this.form.get('origin_date').valueChanges.subscribe(() => this.validateDates())

    }


    public updateKm() {
        this.calcKm()
    }


    selectDate({value}) {
        const date:any = value.format();
        this.minDateDestination = this.helper.date(date).format();
    }

    public searchCity(direction: string = 'from', city: any, control) {
        this.blur = false;
        this.mapsAPILoader.load().then(() => {
            this.form.controls[control].setErrors({ no_place: true });
            let autocomplete = new google.maps.places.Autocomplete(city);
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    let place: any = autocomplete.getPlace();
                    // if() {

                    this.cities[direction] = {
                        lat: parseFloat(place.geometry.location.lat()),
                        lng: parseFloat(place.geometry.location.lng()),
                        address: place.formatted_address
                    }
                    this.form.controls[control].setErrors(null);

                })
            });
        });
    }

    public calcKm() {
        // console.log('calculando')
        setTimeout(() => {
            this.blur = true;
        }, 1000);


        setTimeout(() => {
            if (this.form.get('origin_city').valid && this.form.get('destination_city').valid) {
                // console.log(this.cities)
                // const from = new google.maps.LatLng(this.cities.from.lat, this.cities.from.lng);
                // const to = new google.maps.LatLng(this.cities.to.lat, this.cities.to.lng);
                // const distance = google.maps.geometry.spherical.computeDistanceBetween(from, to);
                // console.log(from, to)
                var directionsService = new google.maps.DirectionsService();

                var request = {
                    origin: this.cities.from.address, // a city, full address, landmark etc
                    destination: this.cities.to.address,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                };

                directionsService.route(request, (response, status) => {
                    this.ngZone.run(() => {
                        if (status == google.maps.DirectionsStatus.OK) {
                            this.form.get('km_distance_public').setValue(response.routes[0].legs[0].distance.text);
                            this.form.get('km_distance').setValue(response.routes[0].legs[0].distance.value);
                        }
                        else {
                            this.additionalCalc()
                        }
                    });
                });
            }
        }, 1000);
    }

    public additionalCalc() {
        const from = new google.maps.LatLng(this.cities.from.lat, this.cities.from.lng);
        const to = new google.maps.LatLng(this.cities.to.lat, this.cities.to.lng);
        let distance: number = google.maps.geometry.spherical.computeDistanceBetween(from, to);
        if(distance) {
            let publicDistance = distance * 0.001;
            this.form.get('km_distance').setValue(distance);
            this.form.get('km_distance_public').setValue( String(publicDistance.toFixed(1) ).replace('.', ',') + ' KM');

        } else {
            this.form.get('km_distance').setValue('');
            this.form.get('km_distance_public').setErrors({ no_routes: true });
        }
    }


    public prepareUpload(input: HTMLInputElement) {
        this.form.get('file').setValue(input.files[0])
    }

    public save() {
        this.loading = true;
        this.calcKm();
        const form = this.form.value;
        form.birthdate = this.helper.date(form.birthdate).format('Y-M-D');
        form.origin_date = this.helper.date(form.origin_date).format('Y-M-D');
        form.destination_date = this.helper.date(form.destination_date).format('Y-M-D');
        form.destination_city = this.cities.to.address;
        form.origin_city = this.cities.from.address;
        const formData = new FormData();
        for (let i in form) {
            if (form[i]) {
                formData.append(i, form[i]);
            }
        }

        this.voucherService.store(formData).subscribe((response: any) => {
            this.router.navigate(['/obrigado'])
        }, (error: HttpErrorResponse) => {
            console.log(error)
        }).add(() => setTimeout(() => this.loading = false, 100));
    }
}
