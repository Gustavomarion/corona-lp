import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    date(date: any = new Date()) {
        return moment(date)
        .parseZone()
        .locale(window.navigator.language);
    }

    public validateCpf(doc: string, digitis: number = 11) {
        let Soma;
        let Resto;

        let isTrue = true;
        Soma = 0;
        if (doc == "00000000000") {
            isTrue = false;
        }
        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(doc.substring(i - 1, i)) * (digitis - i);
        }
        Resto = (Soma * 10) % digitis;

        if ((Resto == 10) || (Resto == digitis)) Resto = 0;
        if (Resto != parseInt(doc.substring(9, 10))) {
            isTrue = false;
        }

        Soma = 0;
        for (let i = 1; i <= 10; i++) {
            Soma = Soma + parseInt(doc.substring(i - 1, i)) * (12 - i);
        }
        Resto = (Soma * 10) % digitis;

        if ((Resto == 10) || (Resto == digitis)) {
            Resto = 0;
        }
        if (Resto != parseInt(doc.substring(10, digitis))) {
            isTrue = false;
        }
        return isTrue;

    }

}
